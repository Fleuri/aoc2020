import string

answer_count = 0
with open("puzzle.txt", 'r') as fd:
    group_answers = set(string.ascii_lowercase)
    lines = fd.readlines()
    for line in lines:
        if line != "\n":
            answer = set(line.strip())
            group_answers = group_answers.intersection(answer)
        else:
            answer_count += len(group_answers)
            print(group_answers)
            group_answers = set(string.ascii_lowercase)
print(answer_count)