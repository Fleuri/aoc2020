answer_count = 0
with open("puzzle.txt", 'r') as fd:
    group_answers = set()
    lines = fd.readlines()
    for line in lines:
        if line != "\n":
            answer = set(line.strip())
            group_answers = group_answers.union(answer)
        else:
            answer_count += len(group_answers)
            print(group_answers)
            group_answers = set()
print(answer_count)