lines = list()
with open('puzzle.txt', 'r') as fd:
    lines = fd.readlines()
lines = list(map(int, lines))
for i, n in enumerate(lines):
    value = n
    lines.remove(value)
    for m in lines:
        if (2020-value-m) in lines:
            x = 2020-value-m
            y = value
            z = m
            print(x*y*z)
            exit(0)
