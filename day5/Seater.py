from math import floor, ceil
import re


def solver(seat_entry: str, lower=0, upper=127, entry_index=0):
    if lower == upper:
        return lower
    elif seat_entry[entry_index] in ['F', 'L']:
        upper = floor((lower+upper)/2)
      #  return row_solver(seat_entry, lower, upper, entry_index)
    elif seat_entry[entry_index] in ['B', 'R']:
        lower = ceil((lower+upper)/2)
    entry_index += 1
    return solver(seat_entry, lower, upper, entry_index)

with open("puzzle.txt", "r") as fd:
    lines = fd.readlines()
    highest_id = 0
    ids = list()
    for line in lines:
        splitted_string = re.findall(r"[F|B]+|[R|L]+", line)
        row = (solver(splitted_string[0], 0, 127))
        seat = (solver(splitted_string[1], 0, 7))
        id = (row * 8 + seat)
        ids.append(id)
        if id > highest_id:
            highest_id = id
        print("Row is " + str(row) + ".", end=' ')
        print("Seat is " + str(seat) + ".", end=' ')
        print("ID is " + str(id) + ".")
    print("Highest id is " + str(highest_id) + ".")
    ids.sort()
    print(ids)
    for i, id in enumerate(ids):
        if i + 1 < len(ids):
            if ids[i + 1] - ids[i] != 1:
                print("Missing id: " + str(ids[i] + 1))