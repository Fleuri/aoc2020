import re


def hgt_validator(value):
    splitted_string = re.findall("\d+|\w+", value)
    if len(splitted_string) != 2:
        return False
    elif splitted_string[1] == 'cm':
        return 150 <= int(splitted_string[0]) <= 193
    elif splitted_string[1] == 'in':
        return 59 <= int(splitted_string[0]) <= 76
    else:
        return False


def eyr_validator(value):
    return 2020 <= int(value) <= 2030


def iyr_validator(value):
    return 2010 <= int(value) <= 2020


def byr_validator(value):
    return 1920 <= int(value) <= 2002


def hcl_validator(value):
    return re.search("^#[\da-f]{6}$", value) is not None


def ecl_validator(value):
    return value in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']

def pid_validator(value):
    return re.search("^\d{9}$", str(value)) is not None

def passport_validator(passport: dict):
    required_fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
    for field in required_fields:
        if not field in passport.keys():
            #print("Invalid passport:" + str(passport) + "length: " + str(len(passport)))
            #print("Missing " + field)
            return False
    #print("Valid passport:" + str(passport) + "length: " + str(len(passport)))
    return True

def passport_validator2(passport: dict):
    required_fields = {'byr': byr_validator, 'iyr': iyr_validator, 'eyr': eyr_validator, 'hgt': hgt_validator,
                       'hcl': hcl_validator, 'ecl': ecl_validator, 'pid': pid_validator}
    for field_key in required_fields.keys():
        if not field_key in passport.keys() or required_fields.get(field_key)(passport.get(field_key)) is False:
            # print("Invalid passport:" + str(passport) + "length: " + str(len(passport)))
            # print("Missing " + field)
            return False
    #print("Valid passport:" + str(passport) + "length: " + str(len(passport)))
    return True

passports = list()
with open("puzzle.txt", 'r') as fd:
    passport = dict()
    lines = fd.readlines()
    for line in lines:
        if line != "\n":
            fields = line.split(' ')
            for field in fields:
                field_list = field.split(':')
                passport[field_list[0]] = field_list[1].strip()
        else:
            passports.append(passport)
            passport = dict()
    passports.append(passport)
    print(len(passports))
    valid_passports = 0
    for passport in passports:
        if passport_validator2(passport):
            valid_passports += 1
    print(valid_passports)