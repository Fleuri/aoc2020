def replace_str_index(text,index=0,replacement=''):
    return '%s%s%s'%(text[:index],replacement,text[index+1:])


def ride_slope(lines, right_movement, print_route=False):
    x = 0
    trees = 0
    for line in lines:
        if x >= len(line):
            x = x - len(line)
        if line[x] == "#":
            trees = trees + 1
        if print_route:
            print(replace_str_index(line, x, "O") + " :" + str(x))
        x = x + right_movement
    return trees


lines = list()
with open('puzzle.txt', 'r') as fd:
    lines = fd.read().splitlines()
    first_slope = ride_slope(lines, 1)
    second_slope = ride_slope(lines, 3)
    third_slope = ride_slope(lines, 5)
    fourth_slope = ride_slope(lines, 7)
    del lines[1::2]
    fifth_slope = ride_slope(lines, 1, True)

    print("1: {} 2: {} 3: {} 4: {} 5: {}".format(first_slope, second_slope, third_slope, fourth_slope, fifth_slope))
    print("Solutions is {}".format(first_slope*second_slope*third_slope*fourth_slope*fifth_slope))
