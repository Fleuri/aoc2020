import re
lines = list()
number_of_valid_inputs_1 = 0
number_of_valid_inputs_2 = 0
with open('puzzle.txt', 'r') as fd:
    lines = fd.readlines()
    for line in lines:
        string = re.split("-|: | ", line)
        char_count = string[3].count(string[2])
        if int(string[0]) <= char_count <= int(string[1]):
            number_of_valid_inputs_1 = number_of_valid_inputs_1 + 1
        if (string[3][int(string[0])-1] == string[2]) != (string[3][int(string[1])-1] == string[2]):
            number_of_valid_inputs_2 = number_of_valid_inputs_2 + 1
    print(number_of_valid_inputs_1)
    print(number_of_valid_inputs_2)

